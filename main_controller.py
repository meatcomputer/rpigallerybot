import readchar
import motor_controller as motoc

mc = motoc.MotorController()
mc.set_speed(50)

def keyprint():
    while True:

        # Read a key
        key = readchar.readkey()
        if(key == 'a'):
            mc.move_counterclockwise()
            print("left turn")
        elif(key == 'w'):
            mc.move_forward()
            print("onward")
        elif(key == 's'):
            mc.move_backward()
            print("backward")
        elif(key == 'd'):
            mc.move_clockwise()
            print("rightturn")
        elif(key == 'x'):
            mc.stop()
            print("stop")
        elif(key == 'q'):
	    break

if __name__ == "__main__":
    keyprint()

