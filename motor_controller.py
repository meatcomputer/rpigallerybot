import RPi.GPIO as GPIO
import time

#motor controller reference https://lastminuteengineers.com/l298n-dc-stepper-driver-arduino-tutorial/
class MotorController:
    def __init__(self):
        self.speed = 70
        GPIO.setmode(GPIO.BCM)
        self.in1_pin = 6
        self.in2_pin = 13
        self.in3_pin = 19
        self.in4_pin = 26
        GPIO.setup(self.in1_pin, GPIO.OUT) 
        GPIO.setup(self.in2_pin, GPIO.OUT)
        GPIO.setup(self.in3_pin, GPIO.OUT) 
        GPIO.setup(self.in4_pin, GPIO.OUT)
        GPIO.setup(18, GPIO.OUT)
        self.pwm = GPIO.PWM(18, 1000)
        self.pwm.start(self.speed)
        self.timeout_start = 0
        self.delay_t = 1 

    def delay_f(self, delay_t):
        if delay_t > 0:
            time.sleep(delay_t)
            self.stop()


    def stop(self):

        GPIO.output(self.in1_pin, GPIO.LOW)
        GPIO.output(self.in2_pin, GPIO.LOW)

        GPIO.output(self.in3_pin, GPIO.LOW)
        GPIO.output(self.in4_pin, GPIO.LOW)

    def move_forward(self):
        GPIO.output(self.in1_pin, GPIO.HIGH)
        GPIO.output(self.in2_pin, GPIO.LOW)

        GPIO.output(self.in3_pin, GPIO.HIGH)
        GPIO.output(self.in4_pin, GPIO.LOW)
        self.delay_f(self.delay_t)


    def move_backward(self):
        GPIO.output(self.in1_pin, GPIO.LOW)
        GPIO.output(self.in2_pin, GPIO.HIGH)

        GPIO.output(self.in3_pin, GPIO.LOW)
        GPIO.output(self.in4_pin, GPIO.HIGH)
        self.delay_f(self.delay_t)

    def move_clockwise(self):
        GPIO.output(self.in1_pin, GPIO.HIGH)
        GPIO.output(self.in2_pin, GPIO.LOW)

        GPIO.output(self.in3_pin, GPIO.LOW)
        GPIO.output(self.in4_pin, GPIO.HIGH)
        self.delay_f(self.delay_t/2)

    def move_counterclockwise(self):
        GPIO.output(self.in1_pin, GPIO.LOW)
        GPIO.output(self.in2_pin, GPIO.HIGH)

        GPIO.output(self.in3_pin, GPIO.HIGH)
        GPIO.output(self.in4_pin, GPIO.LOW)
        self.delay_f(self.delay_t/2)

    def set_speed(self, speed):
        self.pwm.ChangeDutyCycle(speed)


