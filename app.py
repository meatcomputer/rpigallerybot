from flask import Flask, render_template
import motor_controller as moco
mc = moco.MotorController()
mc.stop()

app = Flask(__name__)

@app.route('/')
def main():
    return render_template('main.html')

@app.route('/forward')
def forward():
    mc.move_forward()
    return 'forward'
    

@app.route('/backward')
def backward():
    mc.move_backward()
    return 'back'

@app.route('/right')
def right():
    mc.move_clockwise()
    return 'right'

@app.route('/left')
def left():
    mc.move_counterclockwise()
    return 'left'

@app.route('/stop')
def stopit():
    mc.stop()
    return 'stop'


app.run(host='0.0.0.0', port=80, debug=True)
